import os
import hashlib
import random
import string
import itertools
import time

alfa = "abcdefghijklmnopqrstuvwxyz"
#получение хеша файла
def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()
#получение хеша из строки
def md5_from_string(string):
    hash_md5 = hashlib.md5(string.encode())
    return hash_md5.hexdigest()
#конверт hex в bin
def hex_to_bin(hex):
    bnum = bin(int(hex, 16))
    return bnum[2:]
#конверт строки в bin
def str_to_bin(str):
    binary_converted = ''.join(format(ord(c), 'b') for c in str)
    return binary_converted
#генерация строки
def generate_random_string(length):
    #alfa = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRCTUVWXYZ1234567890-=\!~@#$%^&*()_+|'
    return ''.join(random.choice(alfa) for i in range(length))
#случайное изменение бита в строке
def random_change_one_bit(string):
    length = len(string)
    pos = random.randint(0,length-1)
    if string[pos] == 0:
        r = string[:pos] + str(1) + string[pos+1:]
    else:
        r = string[:pos] + str(0) + string[pos+1:]
    return r
#подсчет измененных битов 
def count_bit(hash_string_bit, hash_newstring_bit):
    len_str = len(hash_string_bit)
    len_newstr = len(hash_newstring_bit)
    count = 0
    if len_str > len_newstr:
        i = 0
        while i < int(len_newstr):
            if hash_newstring_bit[i]!=hash_string_bit[i]: 
                count+=1
            i+=1
    else:
        i=0
        while i < int(len_str):
            if hash_string_bit[i] != hash_newstring_bit[i]:
                count+=1
            i+=1
    return count
#реализация перебора значений по известному хешу и длине строки
def simple_atack(hash, val):
    count = 0
    perm = itertools.product(alfa, repeat=val)
    for i in perm:
        one_str = ''.join(i)
        #print(one_str)
        count+=1
        check_str = md5_from_string(one_str)
        if check_str == hash:
            return str(str(one_str)+"\n"+"кол-во переборов: "+str(count))
            
def theory_count(string, alfa):
    return str(len(alfa)**len(string))

#//////////////////////////////////////////////////////////////////
#основная часть
#меню
end = -1
while end != 0:
    try:
        os.system("cls")
        print("___Hash___")
        print("1. Hash from file")
        print("2. Hash from string")
        print("3. Avalanche effect")
        print("4. Simple atack")
        print("0. Exit")
        command = int(input("Input: "))
        match command:
            case 1:
                os.system("cls")
                print("-Hash from file-")
                filename = input("input filename: ")
                print("file hash: "+str(md5("sam.txt")))
                #print(hex_to_bin(str(md5("sam.txt"))))
                os.system("PAUSE")
                
            case 2:
                os.system("cls")
                print("-Hash from string")
                string = input("input string: ")
                
                print("string hash: "+str(md5_from_string(string)))
                os.system("PAUSE")

            case 3:
                os.system("cls")
                string = generate_random_string(10)
                new_string = random_change_one_bit(str_to_bin(string))
                print("-----------------------------------------------------------------------------------------------")
                print("исходная строка:                          "+string)
                print("")
                print("исходная строка в виде бит:               "+str_to_bin(string))
                print("исходная строка с одним измененным битом: "+new_string)
                print("")
                print("исходная строка в виде хеша:                          "+str(md5_from_string(string)))
                print("исходная строка с одним измененным битом в виде хеша: "+str(md5_from_string(new_string)))
                print("")
                print("исходная строка в виде хеша в битах:                         "+str(hex_to_bin(md5_from_string(string))))
                print("сходная строка с одним измененным битом в виде хеша в битах: "+str(hex_to_bin(md5_from_string(new_string))))
                print("кол-во измененных бит в хеше: "+str(count_bit(str(hex_to_bin(md5_from_string(string))), str(hex_to_bin(md5_from_string(new_string))))))
                os.system("PAUSE")
            case 4:
                os.system("cls")
                val = input("введите кол-во символов в генерируемой последовательности: ")

                #string = "zzz"
                print("начинаем перебор...")
                start = time.time()
                string = generate_random_string(int(val))
                hash_of_string = str(md5_from_string(string))
                find_answer = simple_atack(hash_of_string, int(val))
                os.system("cls")
                print("исходная строка: "+string)
                print("исходный хеш: "+ str(hash_of_string))
                print()
                print("найденная строка с таким же hash: "+str(find_answer))
                end = time.time()
                print("затрачено времени (секунды): "+str(int(end-start)))
                print("общее кол-во возможных переборов: "+theory_count(string, alfa))
                os.system("PAUSE")
            case 0:
                break
    except Exception:
        print("wrong input")
        os.system("PAUSE")
